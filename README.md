If you want to submit a large number of similar jobs, then instead of submitting
them within a cycle with multiple `sbatch`, the good practice would be usage of
job arrays, and then selection of the desired parameters from the id of a task within
the array.

Use the following to download the source on bachelor-login:

```bash
git clone https://gitlab.com/SmirnGreg/bachelor-slurm-jobarray.git
cd bachelor-slurm-jobarray
```

Run the following command:

```bash
sbatch --mail-type=ALL --mail-user=`whoami`@mpia.de --array=1-5,8,12-18:2%3 demo_array.slurm
```

You may remove `--mail` keywords if you don't want notifications.

Also, all the keywords can be embedded into sbatch demo_array.slurm script, just
add them as 
```bash
#SBATCH --array=1-5,8,12-18:2%3
```
It would work same as keyword to sbatch.

It will create a job array with tasks
`1 2 3 4 5 8 12 14 16 18`
and run no more than 3 simultaniously.

Look at the output files and run.sh and demo_array.slurm scripts.

The script must be configured to select input parameters of your code depending 
on the environment variable `$SLURM_ARRAY_TASK_ID`.

For example, if you have 4000 different folders with input files, call them 
inp0, inp1, inp2, etc., and do `cd inp${SLURM_ARRAY_TASK_ID}` before running the
executable within sbatch or srun script. You can run then the script with 
`--array=1-4000%128` (`%128` is for "no more than 128 simultaneous jobs), or 
first test it with `--array=1-10`, and then continue it
with `--array=11-4000%128`. If something went wrong with say task 2129, you can 
rerun it later with `--array=2129`.

Send me smirnov@mpia.de an email if it does not work for you.
